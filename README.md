# cleverbase - login2gether [WIP]

## WIP
 - 2FA expiration within ApiKey
 - frontend app (secrets/authz components)
 - enhance (secrets/authz) api error handling (see test todo)
 - fully build on docker-compose up (it-tests)

## First Login strategy
 - initial created accounts get assigned to the cleverbase service-account approval
 - service-account auto-approves it and generates api key with 1 hour expiration
 - after registration/first login the system enforces mate/2fa approval (WIP)

## Documentation
stack / bootstrapped with  [bootzooka](https://github.com/softwaremill/bootzooka) :

 -  http4s (HTTP library)
 -  Swagger (API docs)
 -  doobie (query SQL typed DSL) + flyway (db schema evolution)
 -  monix (side-effects / concurrency in the backend)
 -  react (frontend)
 -  SQL / PostgreSQL


## backend
- `docker-compose up login2gether-db`

- `export SQL_PASSWORD=b00t200k4`

- `sbt "~backend/reStart"`

API Documentation / Swagger UI -> [http://localhost:8080/api/v1/docs](http://localhost:8080/api/v1/docs)


## frontend
shell:
 - `cd ui`
 - `yarn install`
 - `yarn start`

UI URL -> [http://localhost:3000](http://localhost:3000)

----
### use service-account or register a new one
**user:** service // **email:** service@cleverbase.com

**password:** 12345

**apiKey:** 329cc4a174e04d53870e4daddbafbaf2038b5def63219c7f52da23e61a94b241

