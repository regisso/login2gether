import React from "react";
import { Switch, Route } from "react-router-dom";
import Welcome from "../../pages/Welcome/Welcome";
import Login from "../../pages/Login/Login";
import Register from "../../pages/Register/Register";
import SecretMain from "../../pages/SecretMain/SecretMain";
import Secret from "../../pages/Secret/Secret";
import Profile from "../../pages/Profile/Profile";
import NotFound from "../../pages/NotFound/NotFound";
import ProtectedRoute from "./ProtectedRoute";

const Routes: React.FC = () => (
  <Switch>
    <Route exact path="/">
      <Welcome />
    </Route>
    <Route path="/login">
      <Login />
    </Route>
    <Route path="/register">
      <Register />
    </Route>

    <ProtectedRoute path="/main">
      <SecretMain />
    </ProtectedRoute>
    <ProtectedRoute path="/secret">
      <Secret />
    </ProtectedRoute>
    <ProtectedRoute path="/profile">
      <Profile />
    </ProtectedRoute>

    <Route>
      <NotFound />
    </Route>
  </Switch>
);

export default Routes;
