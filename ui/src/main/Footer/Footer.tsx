import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

interface VersionData {
  buildDate: string;
  buildSha: string;
}

const Footer: React.FC = () => {
  return (
    <Container fluid className="fixed-bottom bg-light text-muted d-none d-sm-block">
      <Row>
        <Container>
          <Row>
            <Col sm={6} className="py-3">
              <small>
                Login2gether - application by <a href="http://github.com/reginaldosoares">Reginaldo Soares</a>,
                <br/> sources available on <a href="https://gitlab.com/regisso/login2gether">GitLab</a>
              </small>
            </Col>
            <Col sm={6} className="text-right py-3">
            </Col>
          </Row>
        </Container>
      </Row>
    </Container>
  );
};

export default Footer;
