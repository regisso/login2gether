import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

const MiddleContent: React.FC = ({ children }) => (
  <Container fluid>
    <Row className="position-relative">
      {children}
    </Row>
  </Container>
);

export default MiddleContent;
