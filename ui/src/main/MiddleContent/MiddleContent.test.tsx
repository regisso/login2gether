import React from "react";
import { render } from "@testing-library/react";
import MiddleContent from "./MiddleContent";

test("renders children", () => {
  const { getByText } = render(<MiddleContent>test children content</MiddleContent>);
  const header = getByText(/test children content/i);
  expect(header).toBeInTheDocument();
});
