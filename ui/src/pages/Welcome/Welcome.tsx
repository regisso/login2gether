import React from "react";
import logo from "./sml-logo-vertical-rgb-trans.png";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { Link } from "react-router-dom";

const Welcome: React.FC = () => (
  <>
    <Container fluid className="py-5 bg-primary text-light text-center">
      <Row>
        <Container>
          <h1>Welcome to Login2gether!</h1>
          <p>
            <Link to="/register" className="btn btn-outline-light">
              Register
            </Link>{" "}
            {" "}or{" "}
            <Link to="/login" className="btn btn-outline-light">
              Login
            </Link>
          </p>
        </Container>
      </Row>
    </Container>
    <Container className="py-5 text-center">
      <p>Cleverbase - Login2gether</p>
      <a href="http://cleverbase.com" rel="noopener noreferrer" target="_blank">
        <Image fluid src={logo} alt="SoftwareMill" width="300" />
      </a>
      <p>
        <br />
        you can browse the{" "}
        <a href="http://localhost:8080/api/v1/docs" target="blank">
          API Documentation
        </a>{" "}
        or{" "} check the{" "}
        <a href="https://gitlab.com/regisso/login2gether" target="blank">
          Source code
        </a>{" "}
        .
      </p>
    </Container>
  </>
);

export default Welcome;
