import React from "react";
import { render } from "@testing-library/react";
import Secret from "./Secret";

test("renders text content", () => {
  const { getByText } = render(<Secret />);
  const header = getByText(/Shhhh, this is a secret place./i);
  expect(header).toBeInTheDocument();
});
