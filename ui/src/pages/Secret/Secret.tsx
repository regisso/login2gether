import React from "react";
import Container from "react-bootstrap/Container";

const Secret: React.FC = () => (
  <>
    <Container className="my-5 text-center">
      <h3>Secret List</h3>
      <p>list of secrets</p>
    </Container>
  </>
);

export default Secret;
