import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {usePromise} from "react-use-promise-matcher";
import authorizationService from "../../services/AuthorizationService/AuthorizationService";
import {UserContext} from "../../contexts/UserContext/UserContext";

interface AuthorizationData {
  id: string;
  sender: string;
  createdOn: string;
}

const Authorization: React.FC = () => {
  const {
    dispatch,
    state: {apiKey, user},
  } = React.useContext(UserContext);

  // @ts-ignore
  // const [result, load] = usePromise<[AuthorizationData], any, any>(() => authorizationService.listPendingAuthz(""));

  // React.useEffect(() => {
  //   load();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  return (
    <>
      <Container className="my-5 text-center">
        <Row>
          <Col md={9} lg={7} xl={6} className="mx-auto">
            <h3>Pending Authorizations</h3>
            <Row><p>auth 1</p></Row>
            <Row><p>auth 2</p></Row>
            <Row><p>auth 3</p></Row>
          </Col>
        </Row>
      </Container>
    </>
  )
};

export default Authorization;
