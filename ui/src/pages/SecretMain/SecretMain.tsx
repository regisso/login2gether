import React from "react";
import Container from "react-bootstrap/Container";
import Authorization from  "./Authorization";

const SecretMain: React.FC = () => (
  <>
    <Container className="my-5 text-center">
      <h3>CleverBase - login2gether</h3>
      <p>You're logged in. Congrats!</p>
      <Authorization/>
    </Container>
  </>
);

export default SecretMain;
