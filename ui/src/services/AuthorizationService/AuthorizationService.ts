import axios, { AxiosRequestConfig } from "axios";
import * as Yup from "yup";

const context = "api/v1/authorization";

const apiKeySchema = Yup.object().required().shape({
  apiKey: Yup.string().required(),
});


const authorizationRequestItem = Yup.object().required().shape({
  id: Yup.string().required(),
  sender: Yup.string().required(),
  createdOn: Yup.string().required(),
});

const authorizationRequest = Yup.array(authorizationRequestItem).required()

const emptySchema = Yup.object().required().shape({});

const listPendingAuthz = (apiKey: string | null) =>
  _securedRequest(apiKey, {
    method: "GET",
    url: context,
  }).then(({ data }) => authorizationRequest.validate(data));



const _securedRequest = (apiKey: string | null, config: AxiosRequestConfig) =>
  axios.request({
    headers: {
      Authorization: `Bearer ${apiKey}`,
    },
    ...config,
  });

export default {
  listPendingAuthz,
};
