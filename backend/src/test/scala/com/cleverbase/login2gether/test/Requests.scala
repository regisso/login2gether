package com.cleverbase.login2gether.test

import com.cleverbase.login2gether.MainModule
import com.cleverbase.login2gether.authorization.AuthorizationApi.{ApproveAuthzRequest, AuthorizationRequest, RejectAuthzRequest}
import com.cleverbase.login2gether.infrastructure.Json._
import com.cleverbase.login2gether.secret.SecretApi.{AddSecret, ShareSecret}
import com.cleverbase.login2gether.user.UserApi._
import monix.eval.Task
import org.http4s._
import org.http4s.syntax.all._

import scala.util.Random

class Requests(val modules: MainModule) extends HttpTestSupport {

  case class RegisteredUser(login: String, email: String, password: String, apiKey: String)

  private val random = new Random()

  def randomLoginEmailPassword(): (String, String, String) =
    (random.nextString(12), s"user${random.nextInt(9000)}@login2gether.com", random.nextString(12))

  // User

  def registerUser(login: String, email: String, password: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/user/register")
      .withEntity(RegisterInfo(login, email, password))

    modules.httpApi.mainRoutes(request).unwrap
  }

  def newRegisteredUsed(): RegisteredUser = {
    val (login, email, password) = randomLoginEmailPassword()
    val apiKey = registerUser(login, email, password).shouldDeserializeTo[Registered].apiKey
    RegisteredUser(login, email, password, apiKey)
  }

  def approve2Factor(apiKey: String): Response[Task] = {
    val response2 = listPendingAuthorization(apiKey)
    val authzReq = response2.shouldDeserializeTo[List[AuthorizationRequest]].head
    approveAuthzRequest(apiKey, authzReq.id)
  }

  def loginUser(loginOrEmail: String, password: String, apiKeyValidHours: Option[Int] = None): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/user/login")
      .withEntity(Login(loginOrEmail, password, apiKeyValidHours))

    modules.httpApi.mainRoutes(request).unwrap
  }

  def getUser(apiKey: String): Response[Task] = {
    val request = Request[Task](method = GET, uri = uri"/user")
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  def changePassword(apiKey: String, password: String, newPassword: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/user/changepassword")
      .withEntity(ChangePassword(password, newPassword))

    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  def updateUser(apiKey: String, login: String, email: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/user")
      .withEntity(UpdateUser(login, email))

    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  // Secret
  def addSecret(apiKey: String, claim: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/secret/add")
      .withEntity(AddSecret(claim))
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  def shareSecret(apiKey: String, secretId: String, userId: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/secret/share")
      .withEntity(ShareSecret(secretId, userId))
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  def listVisibleSecrets(apiKey: String): Response[Task] = {
    val request = Request[Task](method = GET, uri = uri"/secret")
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  // Authorization

  def listPendingAuthorization(apiKey: String): Response[Task] = {
    val request = Request[Task](method = GET, uri = uri"/authorization")
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  def approveAuthzRequest(apiKey: String, authReqId: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/authorization/approve")
      .withEntity(ApproveAuthzRequest(authReqId))
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }

  def rejectAuthzRequest(apiKey: String, authReqId: String): Response[Task] = {
    val request = Request[Task](method = POST, uri = uri"/authorization/reject")
      .withEntity(RejectAuthzRequest(authReqId))
    modules.httpApi.mainRoutes(authorizedRequest(apiKey, request)).unwrap
  }
}
