package com.cleverbase.login2gether.authorization

import com.cleverbase.login2gether.MainModule
import com.cleverbase.login2gether.authorization.AuthorizationApi.AuthorizationRequest
import com.cleverbase.login2gether.config.Config
import com.cleverbase.login2gether.infrastructure.Doobie.Transactor
import com.cleverbase.login2gether.test.{BaseTest, Requests, TestConfig, TestEmbeddedPostgres}
import io.circe.generic.auto.exportDecoder
import monix.eval.Task
import org.http4s.Status
import org.scalatest.concurrent.Eventually
import sttp.client3.SttpBackend
import sttp.client3.impl.monix.TaskMonadAsyncError
import sttp.client3.testing.SttpBackendStub

import java.time.Clock

class AuthorizationApiTest extends BaseTest with TestEmbeddedPostgres with Eventually {
  lazy val modules: MainModule = new MainModule {
    override def xa: Transactor[Task] = currentDb.xa
    override lazy val baseSttpBackend: SttpBackend[Task, Any] = SttpBackendStub(TaskMonadAsyncError)
    override lazy val config: Config = TestConfig
    override lazy val clock: Clock = testClock
  }

  val requests = new Requests(modules)
  import requests._

  "/authorization" should "have a pending authorization after a newly user registration" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val RegisteredUser(_, _, _, apiKey2) = newRegisteredUsed()

    // when
    val response1 = listPendingAuthorization(apiKey)
    val response2 = listPendingAuthorization(apiKey2)

    // then
    response1.status shouldBe Status.Ok
    response1.shouldDeserializeTo[List[AuthorizationRequest]].size shouldBe 1

    response2.status shouldBe Status.Ok
    response2.shouldDeserializeTo[List[AuthorizationRequest]].size shouldBe 1
  }

  "/authorization" should "fail if not authenticated" in {
    val apiKey = "000000"

    // when
    val response1 = listPendingAuthorization(apiKey)

    // then
    response1.status shouldBe Status.Unauthorized
  }

  "/authorization/approve" should "approve a authorization request" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()

    val response1 = listPendingAuthorization(apiKey)
    val authzReq = response1.shouldDeserializeTo[List[AuthorizationRequest]].head

    val response2 = approveAuthzRequest(apiKey, authzReq.id)
    response2.status shouldBe Status.Ok

    val response3 = listPendingAuthorization(apiKey)
    response3.status shouldBe Status.Ok
    response3.shouldDeserializeTo[List[AuthorizationRequest]].size shouldBe 0
  }

  "/authorization/approve" should "not be able to make decision when other recipient targeted" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val RegisteredUser(_, _, _, apiKey2) = newRegisteredUsed()

    val response1 = listPendingAuthorization(apiKey)
    response1.status shouldBe Status.Ok
    val authzReq = response1.shouldDeserializeTo[List[AuthorizationRequest]].head

    val response2 = approveAuthzRequest(apiKey2, authzReq.id)
    // should ignore silently
    response2.status shouldBe Status.Ok

    val response3 = listPendingAuthorization(apiKey)
    response3.status shouldBe Status.Ok
    // should still have one authz request
    response3.shouldDeserializeTo[List[AuthorizationRequest]].size shouldBe 1
  }

  "/authorization/approve" should "fail when trying to approve using invalid apiKey" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val invalidApiKey2 = "123"

    val response1 = listPendingAuthorization(apiKey)
    response1.status shouldBe Status.Ok
    val authzReq = response1.shouldDeserializeTo[List[AuthorizationRequest]].head

    val response2 = approveAuthzRequest(invalidApiKey2, authzReq.id)
    response2.status shouldBe Status.Unauthorized
  }


  "/authorization/reject" should "reject a authorization request" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()

    val response1 = listPendingAuthorization(apiKey)
    val authzReq = response1.shouldDeserializeTo[List[AuthorizationRequest]].head

    val response2 = rejectAuthzRequest(apiKey, authzReq.id)
    response2.status shouldBe Status.Ok

    val response3 = listPendingAuthorization(apiKey)
    response3.status shouldBe Status.Ok
    response3.shouldDeserializeTo[List[AuthorizationRequest]].size shouldBe 0
  }

  "/authorization/reject" should "not be able to make decision when other recipient targeted" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val RegisteredUser(_, _, _, apiKey2) = newRegisteredUsed()

    val response1 = listPendingAuthorization(apiKey)
    response1.status shouldBe Status.Ok
    val authzReq = response1.shouldDeserializeTo[List[AuthorizationRequest]].head

    val response2 = rejectAuthzRequest(apiKey2, authzReq.id)
    // should ignore silently
    response2.status shouldBe Status.Ok

    val response3 = listPendingAuthorization(apiKey)
    response3.status shouldBe Status.Ok
    // should still have one authz request
    response3.shouldDeserializeTo[List[AuthorizationRequest]].size shouldBe 1
  }

  "/authorization/reject" should "fail when trying to approve using invalid apiKey" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val invalidApiKey2 = "123"

    val response1 = listPendingAuthorization(apiKey)
    response1.status shouldBe Status.Ok
    val authzReq = response1.shouldDeserializeTo[List[AuthorizationRequest]].head

    val response2 = rejectAuthzRequest(invalidApiKey2, authzReq.id)
    response2.status shouldBe Status.Unauthorized
  }
}
