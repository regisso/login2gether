package com.cleverbase.login2gether.user

import com.cleverbase.login2gether.MainModule
import com.cleverbase.login2gether.config.Config
import com.cleverbase.login2gether.infrastructure.Doobie._
import com.cleverbase.login2gether.infrastructure.Json._
import com.cleverbase.login2gether.test.{BaseTest, Requests, TestConfig, TestEmbeddedPostgres}
import com.cleverbase.login2gether.user.UserApi._
import monix.eval.Task
import org.http4s.Status
import org.scalatest.concurrent.Eventually
import sttp.client3.SttpBackend
import sttp.client3.impl.monix.TaskMonadAsyncError
import sttp.client3.testing.SttpBackendStub

import java.time.Clock
import scala.concurrent.duration._

class UserApiTest extends BaseTest with TestEmbeddedPostgres with Eventually {
  lazy val modules: MainModule = new MainModule {
    override def xa: Transactor[Task] = currentDb.xa
    override lazy val baseSttpBackend: SttpBackend[Task, Any] = SttpBackendStub(TaskMonadAsyncError)
    override lazy val config: Config = TestConfig
    override lazy val clock: Clock = testClock
  }

  val requests = new Requests(modules)
  import requests._

  "/user/register" should "register" in {
    // given
    val (login, email, password) = randomLoginEmailPassword()

    // when
    val response1 = registerUser(login, email, password)

    // then
    response1.status shouldBe Status.Ok
    val apiKey = response1.shouldDeserializeTo[Registered].apiKey

    // when
    val response4 = getUser(apiKey)

    // then
    response4.shouldDeserializeTo[GetUserOutput].email shouldBe email
  }

  "/user/register" should "not register if data is invalid" in {
    // given
    val (_, email, password) = randomLoginEmailPassword()

    // when
    val response1 = registerUser("x", email, password) // too short

    // then
    response1.status shouldBe Status.BadRequest
    response1.shouldDeserializeToError
  }

  "/user/register" should "not register if email is taken" in {
    // given
    val (login, email, password) = randomLoginEmailPassword()

    // when
    val response1 = registerUser(login + "1", email, password)
    val response2 = registerUser(login + "2", email, password)

    // then
    response1.status shouldBe Status.Ok
    response2.status shouldBe Status.BadRequest
  }

  "/user/login" should "login the user using the login" in {
    // given
    val RegisteredUser(login, _, password, apiKey) = newRegisteredUsed()

    // when
    approve2Factor(apiKey)
    val response1 = loginUser(login, password)

    // then
    response1.shouldDeserializeTo[LoggedIn]
  }

  "/user/login" should "login the user using the email" in {
    // given
    val RegisteredUser(_, email, password, apiKey) = newRegisteredUsed()

    // when
    approve2Factor(apiKey)
    val response1 = loginUser(email, password)

    // then
    response1.shouldDeserializeTo[LoggedIn]
  }

  "/user/login" should "login the user using uppercase email" in {
    // given
    val RegisteredUser(_, email, password, apiKey) = newRegisteredUsed()

    // when
    approve2Factor(apiKey)
    val response1 = loginUser(email.toUpperCase, password)

    // then
    response1.shouldDeserializeTo[LoggedIn]
  }

  "/user/login" should "login the user for the given number of hours" in {
    // given
    val RegisteredUser(login, _, password, apiKeyOrgin) = newRegisteredUsed()

    // when
    approve2Factor(apiKeyOrgin)
    val apiKey = loginUser(login, password, Some(3)).shouldDeserializeTo[LoggedIn].apiKey

    // then
    getUser(apiKey).shouldDeserializeTo[GetUserOutput]

    testClock.forward(2.hours)
    getUser(apiKey).shouldDeserializeTo[GetUserOutput]

    testClock.forward(2.hours)
    getUser(apiKey).status shouldBe Status.Unauthorized
  }

  "/user/login" should "respond with 403 HTTP status code and 'Incorrect login/email or password' message if user was not found" in {
    // given
    val RegisteredUser(_, _, password, _) = newRegisteredUsed()

    // when
    val response = loginUser("unknownLogin", password, Some(3))
    response.status shouldBe Status.Unauthorized
    response.shouldDeserializeToError shouldBe "Incorrect login/email or password"
  }

  "/user/login" should "respond with 403 HTTP status code and 'Incorrect login/email or password' message if password is incorrect for user" in {
    // given
    val RegisteredUser(login, _, _, _) = newRegisteredUsed()

    // when
    val response = loginUser(login, "wrongPassword", Some(3))
    response.status shouldBe Status.Unauthorized
    response.shouldDeserializeToError shouldBe "Incorrect login/email or password"
  }

  "/user/info" should "respond with 403 if the token is invalid" in {
    getUser("invalid").status shouldBe Status.Unauthorized
  }

  "/user/changepassword" should "change the password" in {
    // given
    val RegisteredUser(login, _, password, apiKey) = newRegisteredUsed()
    val newPassword = password + password

    // when
    val response1 = changePassword(apiKey, password, newPassword)

    // then
    response1.shouldDeserializeTo[PasswordChanged]

    approve2Factor(apiKey)
    loginUser(login, password, None).status shouldBe Status.Unauthorized
    loginUser(login, newPassword, None).status shouldBe Status.Ok
  }

  "/user/changepassword" should "not change the password if the current is invalid" in {
    // given
    val RegisteredUser(login, _, password, apiKey) = newRegisteredUsed()
    val newPassword = password + password

    // when
    val response1 = changePassword(apiKey, "invalid", newPassword)

    approve2Factor(apiKey)

    // then
    response1.status shouldBe Status.Unauthorized
    response1.shouldDeserializeToError shouldBe "Incorrect current password"

    loginUser(login, password, None).status shouldBe Status.Ok
    loginUser(login, newPassword, None).status shouldBe Status.Unauthorized
  }

  "/user" should "update the login" in {
    // given
    val RegisteredUser(login, email, _, apiKey) = newRegisteredUsed()
    val newLogin = login + login

    // when
    val response1 = updateUser(apiKey, newLogin, email)

    // then
    response1.shouldDeserializeTo[UserUpdated]
    getUser(apiKey).shouldDeserializeTo[GetUserOutput].login shouldBe newLogin
    getUser(apiKey).shouldDeserializeTo[GetUserOutput].email shouldBe email
  }

  "/user" should "update the email" in {
    // given
    val RegisteredUser(login, _, _, apiKey) = newRegisteredUsed()
    val (_, newEmail, _) = randomLoginEmailPassword()

    // when
    val response1 = updateUser(apiKey, login, newEmail)

    // then
    response1.shouldDeserializeTo[UserUpdated]
    getUser(apiKey).shouldDeserializeTo[GetUserOutput].login shouldBe login
    getUser(apiKey).shouldDeserializeTo[GetUserOutput].email shouldBe newEmail
  }
}
