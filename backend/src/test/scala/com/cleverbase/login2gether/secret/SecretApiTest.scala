package com.cleverbase.login2gether.secret

import com.cleverbase.login2gether.MainModule
import com.cleverbase.login2gether.config.Config
import com.cleverbase.login2gether.infrastructure.Doobie.Transactor
import com.cleverbase.login2gether.secret.SecretApi.SecretItem
import com.cleverbase.login2gether.test.{BaseTest, Requests, TestConfig, TestEmbeddedPostgres}
import io.circe.generic.auto.exportDecoder
import monix.eval.Task
import org.http4s.Status
import org.scalatest.concurrent.Eventually
import sttp.client3.SttpBackend
import sttp.client3.impl.monix.TaskMonadAsyncError
import sttp.client3.testing.SttpBackendStub

import java.time.Clock

class SecretApiTest extends BaseTest with TestEmbeddedPostgres with Eventually {
  lazy val modules: MainModule = new MainModule {
    override def xa: Transactor[Task] = currentDb.xa
    override lazy val baseSttpBackend: SttpBackend[Task, Any] = SttpBackendStub(TaskMonadAsyncError)
    override lazy val config: Config = TestConfig
    override lazy val clock: Clock = testClock
  }

  val requests = new Requests(modules)
  import requests._

  "/secret/add" should "add a new secret" in {

    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val claim = "Secret Content"

    // when
    val response1 = addSecret(apiKey, claim)

    // then
    response1.status shouldBe Status.Ok
  }

  "/secret/add" should "fail if not authenticated" in {
    // given
    val apiKey = "000000"
    val claim = "Secret Content"

    // when
    val response1 = addSecret(apiKey, claim)

    // then
    response1.status shouldBe Status.Unauthorized
  }

  "/secret/share" should "share existent secret" in {

    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val claim = "Shared Secret Content"

    // when
    val response1 = addSecret(apiKey, claim)
    val response2 = listVisibleSecrets(apiKey)

    // then
    response1.status shouldBe Status.Ok
    response2.shouldDeserializeTo[List[SecretItem]]
  }

  //TODO handle with meaningful error
  "/secret/share" should "not share if data is invalid" in {

    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()
    val invalidSecretId = "123"
    val invalidUserId = "321"

    // when
    val response1 = shareSecret(apiKey, invalidSecretId, invalidUserId)

    // then
    response1.status shouldBe Status.InternalServerError
  }

  "/secret/share" should "fail if not authenticated" in {

    // given
    val invalidApiKey = "000"
    val invalidSecretId = "123"
    val invalidUserId = "321"

    // when
    val response1 = shareSecret(invalidApiKey, invalidSecretId, invalidUserId)

    // then
    response1.status shouldBe Status.Unauthorized
  }

  "/secret" should "list visible secrets" in {
    // given
    val RegisteredUser(_, _, _, apiKey) = newRegisteredUsed()

    // when
    val response1 = addSecret(apiKey, "Shared Secret Content List")
    val response2 = addSecret(apiKey, "Shared Secret Content List 2")

    val response3 = listVisibleSecrets(apiKey)

    // then
    response1.status shouldBe Status.Ok
    response2.status shouldBe Status.Ok

    response3.shouldDeserializeTo[List[SecretItem]].size shouldBe 2
  }
}
