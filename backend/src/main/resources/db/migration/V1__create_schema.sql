-- USERS
CREATE TABLE "users"
(
  "id"              TEXT        NOT NULL,
  "login"           TEXT        NOT NULL,
  "login_lowercase" TEXT        NOT NULL,
  "email_lowercase" TEXT        NOT NULL,
  "password"        TEXT        NOT NULL,
  "created_on"      TIMESTAMPTZ NOT NULL
);
ALTER TABLE "users"
  ADD CONSTRAINT "users_id" PRIMARY KEY ("id");
CREATE UNIQUE INDEX "users_login_lowercase" ON "users" ("login_lowercase");
CREATE UNIQUE INDEX "users_email_lowercase" ON "users" ("email_lowercase");

-- API KEYS
CREATE TABLE "api_keys"
(
  "id"          TEXT        NOT NULL,
  "user_id"     TEXT        NOT NULL,
  "created_on"  TIMESTAMPTZ NOT NULL,
  "valid_until" TIMESTAMPTZ NOT NULL
);
ALTER TABLE "api_keys"
  ADD CONSTRAINT "api_keys_id" PRIMARY KEY ("id");
ALTER TABLE "api_keys"
  ADD CONSTRAINT "api_keys_user_id_fk" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;


-- AUTH_REQUESTS
create table "auth_requests"
(
    "user_id_sender" TEXT
        constraint "auth_requests_users_id_fk"
            references "users"
            on update cascade on delete cascade,
    "user_id_recipient" TEXT
        constraint "auth_requests_users_id_fk_2"
            references "users"
            on update cascade on delete cascade,
    "created_on" timestamp with time zone not null,
    "updated_on" timestamp with time zone not null,
    "id" TEXT not null
        constraint "auth_requests_pk"
            primary key,
    "status" integer default 0 not null
);

comment on column auth_requests.status is '0 = pending
1 = approved
2 = rejected';

-- SECRETS
create table secrets
(
    id text not null
        constraint secrets_pk
            primary key,
    user_id_owner text
        constraint secrets_users_id_fk
            references users
            on update cascade on delete cascade,
    claim text,
    created_on timestamp with time zone not null
);

create table secrets_permissions
(
    id text not null
        constraint secrets_permissions_pk
            primary key,
    user_id text not null
        constraint secrets_permissions_users_id_fk
            references users
            on update cascade on delete cascade,
    created_on timestamp with time zone not null,
    secret_id text
        constraint secrets_permissions_secrets_id_fk
            references secrets
            on update cascade on delete cascade
);

