package com.cleverbase.login2gether.config

import com.cleverbase.login2gether.http.HttpConfig
import com.cleverbase.login2gether.infrastructure.DBConfig
import com.cleverbase.login2gether.user.UserConfig

/**
  * Maps to the `application.conf` file. Configuration for all modules of the application.
  */
case class Config(db: DBConfig, api: HttpConfig, user: UserConfig)
