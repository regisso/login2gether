package com.cleverbase.login2gether.config

case class Sensitive(value: String) extends AnyVal {
  override def toString: String = "***"
}
