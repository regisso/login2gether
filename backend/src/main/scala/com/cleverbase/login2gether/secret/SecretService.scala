package com.cleverbase.login2gether.secret

import com.cleverbase.login2gether.infrastructure.Doobie.ConnectionIO
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.{Id, IdGenerator}
import com.softwaremill.tagging.@@
import com.typesafe.scalalogging.StrictLogging
import monix.execution.Scheduler.Implicits.global

import java.time.Clock

class SecretService(
    secretModel: SecretModel,
    idGenerator: IdGenerator,
    clock: Clock
) extends StrictLogging {

  def create(userId: Id @@ User, claim: String): ConnectionIO[Unit] = {
    for {
      idClaim <- idGenerator.nextId[SecretClaim]().to[ConnectionIO]
      now = clock.instant()
      _ <- secretModel.insert(SecretClaim(idClaim, userId, claim, now))
      idSecPerm <- idGenerator.nextId[SecretPermission]().to[ConnectionIO]
      _ <- secretModel.insertPermission(SecretPermission(idSecPerm, idClaim, userId, now))
    } yield ()
  }

  def share(userId: Id @@ User, idClaim: Id @@ SecretClaim): ConnectionIO[Unit] = {
    for {
      idSecPerm <- idGenerator.nextId[SecretPermission]().to[ConnectionIO]
      now = clock.instant()
      _ <- secretModel.insertPermission(SecretPermission(idSecPerm, idClaim, userId, now))
    } yield ()
  }

  def listVisibleByUser(id: Id @@ User): ConnectionIO[List[Secret]] = secretModel.findWithPermission(id)
}
