package com.cleverbase.login2gether.secret

import cats.data.NonEmptyList
import com.cleverbase.login2gether.http.Http
import com.cleverbase.login2gether.infrastructure.Doobie._
import com.cleverbase.login2gether.infrastructure.Json._
import com.cleverbase.login2gether.security.{ApiKey, Auth}
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.{RichString, ServerEndpoints}
import doobie.util.transactor.Transactor
import monix.eval.Task

class SecretApi(http: Http, auth: Auth[ApiKey], secretService: SecretService, xa: Transactor[Task]) {
  import SecretApi._
  import http._

  private val SecretPath = "secret"

  private val createSecretEndpoint = secureEndpoint.post
    .in(SecretPath / "add")
    .in(jsonBody[AddSecret])
    .out(jsonBody[SecretCreated])
    .serverLogic { case (authData, data) =>
      (for {
        userId <- auth(authData)
        _ <- secretService.create(userId, data.claim).transact(xa)
      } yield SecretCreated()).toOut
    }

  private val shareSecretEndpoint = secureEndpoint.post
    .in(SecretPath / "share")
    .in(jsonBody[ShareSecret])
    .out(jsonBody[SecretShared])
    .serverLogic { case (authData, data) =>
      (for {
        _ <- auth(authData)
        _ <- secretService.share(data.userId.asId[User], data.id.asId[SecretClaim]).transact(xa)
      } yield SecretShared()).toOut
    }

  private val listVisibleSecrets = secureEndpoint.get
    .in(SecretPath)
    .out(jsonBody[List[SecretItem]])
    .serverLogic { authData =>
      (for {
        userId <- auth(authData)
        reqs <- secretService.listVisibleByUser(userId).transact(xa)
      } yield reqs.map(r => SecretItem(r.id, r.claim, r.owner))).toOut
    }

  val endpoints: ServerEndpoints =
    NonEmptyList
      .of(
        listVisibleSecrets,
        createSecretEndpoint,
        shareSecretEndpoint
      )
      .map(_.tag("secret"))
}

object SecretApi {
  case class ShareSecret(id: String, userId: String)
  case class SecretShared()

  case class AddSecret(claim: String)
  case class SecretCreated()

  case class SecretItem(id: String, claim: String, owner: String)
}
