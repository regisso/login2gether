package com.cleverbase.login2gether.secret

import com.cleverbase.login2gether.http.Http
import com.cleverbase.login2gether.security.{ApiKey, Auth}
import com.cleverbase.login2gether.util.BaseModule
import doobie.util.transactor.Transactor
import monix.eval.Task

trait SecretModule extends BaseModule {
  lazy val secretModel = new SecretModel
  lazy val secretApi = new SecretApi(http, apiKeyAuth, secretService, xa)
  lazy val secretService = new SecretService(secretModel, idGenerator, clock)

  def xa: Transactor[Task]
  def http: Http
  def apiKeyAuth: Auth[ApiKey]
}
