package com.cleverbase.login2gether.secret

import cats.implicits._
import com.cleverbase.login2gether.infrastructure.Doobie.{ConnectionIO, _}
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.Id
import com.softwaremill.tagging.@@

import java.time.Instant

class SecretModel {

  def insert(secret: SecretClaim): ConnectionIO[Unit] = {
    sql"""INSERT INTO secrets (id, user_id_owner, claim, created_on)
         |VALUES (${secret.id}, ${secret.userIdOwner}, ${secret.claim}, ${secret.createdOn})""".stripMargin.update.run.void
  }

  def insertPermission(secret: SecretPermission): ConnectionIO[Unit] = {
    sql"""INSERT INTO secrets_permissions (id, secret_id, user_id, created_on)
         |VALUES (${secret.id}, ${secret.secretId}, ${secret.userId}, ${secret.createdOn})""".stripMargin.update.run.void
  }

  def findWithPermission(id: Id @@ User): ConnectionIO[List[Secret]] = {
    sql"""SELECT s.id, s.claim, u.login AS owner FROM secrets AS s
           |INNER JOIN secrets_permissions sp ON s.id = sp.secret_id
           |INNER JOIN users u ON u.id = s.user_id_owner
           |WHERE sp.user_id = $id""".stripMargin
      .query[Secret]
      .to[List]
  }

}

case class Secret(
    id: Id @@ SecretClaim,
    claim: String,
    owner: String
)

case class SecretClaim(
    id: Id @@ SecretClaim,
    userIdOwner: Id @@ User,
    claim: String,
    createdOn: Instant
)

case class SecretPermission(
    id: Id @@ SecretPermission,
    secretId: Id @@ SecretClaim,
    userId: Id @@ User,
    createdOn: Instant
)
