package com.cleverbase.login2gether

import com.cleverbase.login2gether.authorization.AuthorizationModule
import com.cleverbase.login2gether.http.{Http, HttpApi}
import com.cleverbase.login2gether.infrastructure.InfrastructureModule
import com.cleverbase.login2gether.secret.SecretModule
import com.cleverbase.login2gether.security.SecurityModule
import com.cleverbase.login2gether.user.UserModule
import com.cleverbase.login2gether.util.{DefaultIdGenerator, IdGenerator, ServerEndpoints}

import java.time.Clock

/** Main application module. Depends on resources initialised in [[InitModule]].
  */
trait MainModule extends SecurityModule with UserModule with AuthorizationModule with SecretModule with InfrastructureModule {

  override lazy val idGenerator: IdGenerator = DefaultIdGenerator
  override lazy val clock: Clock = Clock.systemUTC()

  lazy val http: Http = new Http()

  private lazy val endpoints: ServerEndpoints =
    userApi.endpoints concatNel authorizationApi.endpoints concatNel secretApi.endpoints

  lazy val httpApi: HttpApi = new HttpApi(http, endpoints, config.api)

}
