package com.cleverbase.login2gether.authorization
import cats.implicits._
import com.cleverbase.login2gether.infrastructure.Doobie._
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.Id
import com.softwaremill.tagging.@@

import java.time.Instant

class AuthorizationModel {

  def insert(authRequest: AuthRequest): ConnectionIO[Unit] = {
    sql"""INSERT INTO auth_requests (id, user_id_sender, user_id_recipient, created_on, updated_on, status)
         |VALUES (${authRequest.id}, ${authRequest.userIdSender}, ${authRequest.userIdRecipient},${authRequest.createdOn},
         |${authRequest.updatedOn}, ${authRequest.status})""".stripMargin.update.run.void
  }

  def updateStatus(recipient: Id @@ User, authRequestId: Id @@ AuthRequest, status: RequestStatus, updatedOn: Instant): ConnectionIO[Unit] =
    sql"""UPDATE auth_requests
         |SET status = ${status.value}, updated_on = $updatedOn
         |WHERE id = $authRequestId AND user_id_recipient = $recipient""".stripMargin.update.run.void

  def findLatestRequest(sender: Id @@ User): ConnectionIO[Option[AuthRequest]] = {
    sql"""SELECT id, user_id_sender, user_id_recipient, status, created_on, updated_on
         |FROM auth_requests
         |WHERE user_id_sender = $sender
         |ORDER BY created_on""".stripMargin
      .query[AuthRequest]
      .option
  }

  def findByUserAndStatus(recipient: Id @@ User, status: RequestStatus): ConnectionIO[List[AuthUserRequest]] =
    sql"""SELECT ar.id, u.login, u.email_lowercase, ar.created_on, updated_on, status
         |FROM auth_requests as ar
         |INNER JOIN users u on u.id = ar.user_id_sender
         |WHERE user_id_recipient = $recipient AND status = ${status.value}""".stripMargin
      .query[AuthUserRequest]
      .to[List]
}

case class AuthUserRequest(
    id: Id @@ AuthRequest,
    login: String,
    email: String,
    createdOn: Instant,
    updatedOn: Instant,
    status: Int
//    apiKeyId: Id @@ ApiKey
)
case class AuthRequest(
    id: Id @@ AuthRequest,
    userIdSender: Id @@ User,
    userIdRecipient: Id @@ User,
    status: Int,
    createdOn: Instant,
    updatedOn: Instant
)

sealed trait RequestStatus {
  def value: Int
}
case object PENDING extends RequestStatus {
  override def value: Int = 0
}
case object APPROVED extends RequestStatus {
  override def value: Int = 1
}
case object REJECTED extends RequestStatus {
  override def value: Int = 2
}
