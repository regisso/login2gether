package com.cleverbase.login2gether.authorization

import com.cleverbase.login2gether.http.Http
import com.cleverbase.login2gether.security.{ApiKey, Auth}
import com.cleverbase.login2gether.util.BaseModule
import doobie.util.transactor.Transactor
import monix.eval.Task

trait AuthorizationModule extends BaseModule {
  lazy val authorizationModel = new AuthorizationModel
  lazy val authorizationApi = new AuthorizationApi(http, apiKeyAuth, authorizationService, xa)
  lazy val authorizationService = new AuthorizationService(authorizationModel, idGenerator, clock)

  def xa: Transactor[Task]
  def http: Http
  def apiKeyAuth: Auth[ApiKey]
}
