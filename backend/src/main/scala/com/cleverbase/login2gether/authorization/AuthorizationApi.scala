package com.cleverbase.login2gether.authorization

import cats.data.NonEmptyList
import com.cleverbase.login2gether.http.Http
import com.cleverbase.login2gether.infrastructure.Doobie._
import com.cleverbase.login2gether.infrastructure.Json._
import com.cleverbase.login2gether.security.{ApiKey, Auth}
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.ServerEndpoints
import doobie.util.transactor.Transactor
import monix.eval.Task
import com.cleverbase.login2gether.util._

import java.time.Instant

class AuthorizationApi(http: Http, auth: Auth[ApiKey], authzService: AuthorizationService, xa: Transactor[Task]) {
  import AuthorizationApi._
  import http._

  private val AuthRequestPath = "authorization"

  private val listPendingAuthzEndpoint = secureEndpoint.get
    .in(AuthRequestPath)
    .out(jsonBody[List[AuthorizationRequest]])
    .serverLogic { authData =>
      (for {
        userId <- auth(authData)
        reqs <- authzService.getPendingRequests(userId).transact(xa)
      } yield reqs.map(r => AuthorizationRequest(r.id, s"(${r.login}) - ${r.email}", r.createdOn))).toOut
    }


  private val newAuthzEndpoint = secureEndpoint.post
    .in(AuthRequestPath)
    .in(jsonBody[NewAuthzRequest])
    .out(jsonBody[AuthorizationAck])
    .serverLogic {
      case (authData, data) =>
        (for {
          userId <- auth(authData)
          _ <- authzService.create(userId.asId[User], data.recipientUserId.asId[User]).transact(xa)
        } yield AuthorizationAck()).toOut
    }


  private val approvalAuthzEndpoint = secureEndpoint.post
    .in(AuthRequestPath / "approve")
    .in(jsonBody[ApproveAuthzRequest])
    .out(jsonBody[AuthorizationAck])
    .serverLogic {
      case (authData, data) =>
        (for {
          userId <- auth(authData)
          _ <- authzService.approve(userId, data.authReqId.asId[AuthRequest]).transact(xa)
        } yield AuthorizationAck()).toOut
    }

  private val rejectAuthzEndpoint = secureEndpoint.post
    .in(AuthRequestPath / "reject")
    .in(jsonBody[RejectAuthzRequest])
    .out(jsonBody[AuthorizationAck])
    .serverLogic {
      case (authData, data) =>
        (for {
          userId <- auth(authData)
          _ <- authzService.reject(userId, data.authReqId.asId[AuthRequest]).transact(xa)
        } yield AuthorizationAck()).toOut
    }

  val endpoints: ServerEndpoints =
    NonEmptyList
      .of(
        listPendingAuthzEndpoint,
        approvalAuthzEndpoint,
        rejectAuthzEndpoint,
        newAuthzEndpoint
      )
      .map(_.tag("authorization"))
}

object AuthorizationApi {
  case class NewAuthzRequest(recipientUserId: String)
  case class ApproveAuthzRequest(authReqId: String)
  case class RejectAuthzRequest(authReqId: String)
  case class AuthorizationAck()

  case class AuthorizationRequest(id: String, sender: String, createdOn: Instant)
}
