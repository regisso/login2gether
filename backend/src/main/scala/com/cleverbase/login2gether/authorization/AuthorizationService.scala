package com.cleverbase.login2gether.authorization

import com.cleverbase.login2gether.infrastructure.Doobie.ConnectionIO
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.{Id, IdGenerator}
import com.softwaremill.tagging.@@
import com.typesafe.scalalogging.StrictLogging
import monix.execution.Scheduler.Implicits.global

import java.time.Clock

class AuthorizationService(
    authRequestModel: AuthorizationModel,
    idGenerator: IdGenerator,
    clock: Clock
) extends StrictLogging {

  def create(sender: Id @@ User, recipient: Id @@ User): ConnectionIO[Unit] = {
    for {
      id <- idGenerator.nextId[AuthRequest]().to[ConnectionIO]
      now = clock.instant()
      _ <- authRequestModel.insert(AuthRequest(id, sender, recipient, PENDING.value, now, now))
    } yield ()
  }

  def approve(user: Id @@ User, authRequestId: Id @@ AuthRequest): ConnectionIO[Unit] =
    makeDecision(user, authRequestId, APPROVED)

  def reject(user: Id @@ User, authRequestId: Id @@ AuthRequest): ConnectionIO[Unit] =
    makeDecision(user, authRequestId, REJECTED)

  def getRequestStatus(sender: Id @@ User): ConnectionIO[Option[AuthRequest]] =
    authRequestModel.findLatestRequest(sender)

  def getPendingRequests(recipientUserId: Id @@ User): ConnectionIO[List[AuthUserRequest]] =
    authRequestModel.findByUserAndStatus(recipient = recipientUserId, status = PENDING)

  private def makeDecision(user: Id @@ User, authRequestId: Id @@ AuthRequest, decision: RequestStatus): ConnectionIO[Unit] =
    authRequestModel.updateStatus(user, authRequestId, decision, clock.instant())

}
