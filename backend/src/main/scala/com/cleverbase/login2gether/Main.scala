package com.cleverbase.login2gether

import com.cleverbase.login2gether.config.Config
import com.typesafe.scalalogging.StrictLogging
import doobie.util.transactor
import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import sttp.client3.SttpBackend

object Main extends StrictLogging {
  def main(args: Array[String]): Unit = {
    Thread.setDefaultUncaughtExceptionHandler((t, e) => logger.error("Uncaught exception in thread: " + t, e))

    val initModule = new InitModule {}
    initModule.logConfig()

    val mainTask = initModule.db.transactorResource.use { _xa =>
      initModule.baseSttpBackend.use { _baseSttpBackend =>
        val modules = new MainModule {
          override def xa: transactor.Transactor[Task] = _xa
          override def baseSttpBackend: SttpBackend[Task, Any] = _baseSttpBackend
          override def config: Config = initModule.config
        }

        modules.httpApi.resource.use(_ => Task.never)
      }
    }
    mainTask.runSyncUnsafe()
  }
}
