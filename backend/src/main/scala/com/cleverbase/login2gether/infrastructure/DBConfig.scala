package com.cleverbase.login2gether.infrastructure

import com.cleverbase.login2gether.config.Sensitive

case class DBConfig(username: String, password: Sensitive, url: String, migrateOnStart: Boolean, driver: String, connectThreadPoolSize: Int)
