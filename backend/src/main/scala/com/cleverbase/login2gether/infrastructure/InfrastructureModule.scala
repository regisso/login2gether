package com.cleverbase.login2gether.infrastructure

import monix.eval.Task
import sttp.client3.SttpBackend

trait InfrastructureModule {
  def baseSttpBackend: SttpBackend[Task, Any]
}
