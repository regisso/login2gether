package com.cleverbase.login2gether.security

import com.cleverbase.login2gether.infrastructure.Doobie._
import com.cleverbase.login2gether.user.User
import com.cleverbase.login2gether.util.{Id, IdGenerator}
import com.softwaremill.tagging.@@
import com.typesafe.scalalogging.StrictLogging
import monix.execution.Scheduler.Implicits.global

import java.time.temporal.ChronoUnit
import java.time.{Clock, Instant}
import scala.concurrent.duration.Duration

class ApiKeyService(apiKeyModel: ApiKeyModel, idGenerator: IdGenerator, clock: Clock) extends StrictLogging {

  def create(userId: Id @@ User, valid: Duration): ConnectionIO[ApiKey] = {
    val now = clock.instant()
    val validUntil = now.plus(valid.toMinutes, ChronoUnit.MINUTES)
    for {
      id <- idGenerator.nextId[ApiKey]().to[ConnectionIO]
      apiKey = ApiKey(id, userId, now, validUntil)
      _ = logger.debug(s"Creating a new api key for user $userId, valid until: $validUntil")
      apiKey <- apiKeyModel.insert(apiKey).map(_ => apiKey)
    } yield apiKey

  }
}

case class ApiKey(id: Id @@ ApiKey, userId: Id @@ User, createdOn: Instant, validUntil: Instant)
