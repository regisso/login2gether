package com.cleverbase.login2gether.util

import java.time.Clock

import com.cleverbase.login2gether.config.Config

trait BaseModule {
  def idGenerator: IdGenerator
  def clock: Clock
  def config: Config
}
