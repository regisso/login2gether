package com.cleverbase.login2gether.user

import com.cleverbase.login2gether.authorization.AuthorizationService
import com.cleverbase.login2gether.http.Http
import com.cleverbase.login2gether.security.{ApiKey, ApiKeyService, Auth}
import com.cleverbase.login2gether.util.BaseModule
import doobie.util.transactor.Transactor
import monix.eval.Task

trait UserModule extends BaseModule {
  lazy val userModel = new UserModel
  lazy val userApi = new UserApi(http, apiKeyAuth, userService, xa)
  lazy val userService =
    new UserService(userModel, authorizationService, apiKeyService, idGenerator, clock, config.user)

  def authorizationService: AuthorizationService
  def http: Http
  def apiKeyAuth: Auth[ApiKey]
  def apiKeyService: ApiKeyService
  def xa: Transactor[Task]
}
