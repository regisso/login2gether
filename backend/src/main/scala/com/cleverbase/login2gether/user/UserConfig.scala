package com.cleverbase.login2gether.user

import scala.concurrent.duration.Duration

case class UserConfig(defaultApiKeyValid: Duration)
