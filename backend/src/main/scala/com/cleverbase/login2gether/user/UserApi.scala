package com.cleverbase.login2gether.user

import java.time.Instant

import cats.data.NonEmptyList
import com.cleverbase.login2gether.http.Http
import com.cleverbase.login2gether.infrastructure.Json._
import com.cleverbase.login2gether.infrastructure.Doobie._
import com.cleverbase.login2gether.security.{ApiKey, Auth}
import com.cleverbase.login2gether.util.ServerEndpoints
import doobie.util.transactor.Transactor
import monix.eval.Task

import scala.concurrent.duration._

class UserApi(http: Http, auth: Auth[ApiKey], userService: UserService, xa: Transactor[Task]) {
  import UserApi._
  import http._

  private val UserPath = "user"

  private val registerUserEndpoint = baseEndpoint.post
    .in(UserPath / "register")
    .in(jsonBody[RegisterInfo])
    .out(jsonBody[Registered])
    .serverLogic { data =>
      (for {
        apiKey <- userService.registerNewUser(data.login, data.email, data.password).transact(xa)
      } yield Registered(apiKey.id)).toOut
    }

  private val loginEndpoint = baseEndpoint.post
    .in(UserPath / "login")
    .in(jsonBody[Login])
    .out(jsonBody[LoggedIn])
    .serverLogic { data =>
      (for {
        apiKey <- userService
          .login(data.loginOrEmail, data.password, data.apiKeyValidHours.map(h => Duration(h.toLong, HOURS)))
          .transact(xa)
      } yield LoggedIn(apiKey.id)).toOut
    }

  private val changePasswordEndpoint = secureEndpoint.post
    .in(UserPath / "changepassword")
    .in(jsonBody[ChangePassword])
    .out(jsonBody[PasswordChanged])
    .serverLogic {
      case (authData, data) =>
        (for {
          userId <- auth(authData)
          _ <- userService.changePassword(userId, data.currentPassword, data.newPassword).transact(xa)
        } yield PasswordChanged()).toOut
    }

  private val getUserEndpoint = secureEndpoint.get
    .in(UserPath)
    .out(jsonBody[GetUserOutput])
    .serverLogic { authData =>
      (for {
        userId <- auth(authData)
        user <- userService.findById(userId).transact(xa)
      } yield GetUserOutput(user.login, user.emailLowerCased, user.createdOn)).toOut
    }

  private val updateUserEndpoint = secureEndpoint.post
    .in(UserPath)
    .in(jsonBody[UpdateUser])
    .out(jsonBody[UserUpdated])
    .serverLogic {
      case (authData, data) =>
        (for {
          userId <- auth(authData)
          _ <- userService.changeUser(userId, data.login, data.email).transact(xa)
        } yield UserUpdated()).toOut
    }

  val endpoints: ServerEndpoints =
    NonEmptyList
      .of(
        registerUserEndpoint,
        loginEndpoint,
        changePasswordEndpoint,
        getUserEndpoint,
        updateUserEndpoint
      )
      .map(_.tag("user"))
}

object UserApi {

  case class RegisterInfo(login: String, email: String, password: String)
  case class Registered(apiKey: String)

  case class ChangePassword(currentPassword: String, newPassword: String)
  case class PasswordChanged()

  case class Login(loginOrEmail: String, password: String, apiKeyValidHours: Option[Int])
  case class LoggedIn(apiKey: String)

  case class UpdateUser(login: String, email: String)
  case class UserUpdated()

  case class GetUserOutput(login: String, email: String, createdOn: Instant)
}
