package com.cleverbase.login2gether.http

case class HttpConfig(host: String, port: Int)
